module.exports = {
    testEnvironment: 'jsdom',
    moduleNameMapper: {
      "\\.(scss)$": "identity-obj-proxy",
      
    },
    clearMocks: true,
    transform: {
      '^.+\\.tsx?$': 'ts-jest'
    },
    setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect']
  }