import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';

function renderWithRouter(children: any): any {
  return render(<Router history={createMemoryHistory()}>{children}</Router>);
}

export default renderWithRouter;
