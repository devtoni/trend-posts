import React from 'react';
import { screen, act } from '@testing-library/react';
import PostList from '../../src/components/PostList/PostList';
import renderWithRouter from '../helpers/renderWithRouter';

const mockedService = {
  execute: jest.fn().mockImplementation(() => Promise.resolve(posts)),
};

jest.mock('../../src/services/GetPosts', () => ({
  __esModule: true,
  default: () => mockedService,
}));

describe('PostList', () => {
  it('Should show a list of posts', async () => {
    await act(async () => {
      await renderWithRouter(<PostList />);
    });

    const [firstPost, secondPost] = posts;

    expect(screen.getByText(firstPost.title)).toBeInTheDocument();
    expect(screen.getByText(secondPost.title)).toBeInTheDocument();
  });
});

const posts = [
  {
    id: 1,
    title: 'title-1',
    body: 'body-1',
  },
  {
    id: 2,
    title: 'title-2',
    body: 'body-2',
  },
];
