import React from 'react';
import { screen } from '@testing-library/react';
import PostCard from '../../src/components/PostCard/PostCard';
import renderWithRouter from '../helpers/renderWithRouter';

describe('PostCard', () => {
  it('Should render the expected content', () => {
    renderWithRouter(<PostCard post={post} />);

    expect(screen.getByText(/title-1/i)).toBeInTheDocument();
    expect(screen.getByText(/body-1/)).toBeInTheDocument();
  });
});

const post = {
  id: 1,
  title: 'title-1',
  body: 'body-1',
};
