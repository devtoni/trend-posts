import React from 'react';
import { render, screen } from '@testing-library/react';
import PostComment from '../../src/components/PostComment/PostComment';

describe('PostComment', () => {
  it('Should render the expected content', () => {
    render(<PostComment comment={comment} />);

    expect(screen.getByText(/email@email.com/i)).toBeInTheDocument();
    expect(screen.getByText(/body-1/)).toBeInTheDocument();
  });
});

const comment = {
  id: 1,
  email: 'email@email.com',
  body: 'body-1',
};
