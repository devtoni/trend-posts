import React from 'react';
import { render, screen, act } from '@testing-library/react';
import PostDetail from '../../src/components/PostDetail/PostDetail';
import renderWithRouter from '../helpers/renderWithRouter';

const mockedService = {
  execute: jest.fn().mockImplementation(() => Promise.resolve(postDetail)),
};

jest.mock('../../src/services/GetPostDetailById', () => ({
  __esModule: true,
  default: () => mockedService,
}));

describe('PostDetail', () => {
  it('Should show a detailed post', async () => {
    const postId = '1';

    await act(async () => {
      await renderWithRouter(<PostDetail postId={postId} />);
    });

    expect(screen.getByText(postDetail.title)).toBeInTheDocument();
    expect(screen.getByText(postDetail.body)).toBeInTheDocument();
    expect(screen.getByText(postDetail.comments[0].email)).toBeInTheDocument();
  });
});

const postDetail = {
  id: 1,
  title: 'title-1',
  body: 'body-1',
  comments: [
    {
      id: 1,
      email: 'Eliseo@gardner.biz',
      body: 'laudantium',
    },
  ],
};
