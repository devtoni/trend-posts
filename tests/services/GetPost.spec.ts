import { HttpClient } from '../../src/models/http/HttpClient';
import { GetPosts } from '../../src/services/GetPosts';

describe('GetPost', () => {
  it('should return a list of posts', async () => {
    const httpClient = {
      get: jest.fn().mockResolvedValue(posts),
    } as HttpClient;
    const getPostsService = new GetPosts(httpClient);

    const actualPosts = await getPostsService.execute();

    expect(actualPosts).toEqual(expectedPosts);
  });
});

const posts = [
  {
    userId: 1,
    id: 1,
    title: 'title-1',
    body: 'body-1',
  },
  {
    userId: 1,
    id: 2,
    title: 'title-2',
    body: 'body-2',
  },
];

const expectedPosts = [
  {
    id: 1,
    title: 'title-1',
    body: 'body-1',
  },
  {
    id: 2,
    title: 'title-2',
    body: 'body-2',
  },
];
