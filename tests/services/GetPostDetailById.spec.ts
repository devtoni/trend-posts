import { HttpClient } from '../../src/models/http/HttpClient';
import { GetPostDetailById } from '../../src/services/GetPostDetailById';

describe('GetPostDetailById', () => {
  it('should return a detailed post', async () => {
    const httpClient = {
      get: jest
        .fn()
        .mockResolvedValueOnce(post)
        .mockResolvedValueOnce(comments),
    } as HttpClient;
    const getPostDetailByIdService = new GetPostDetailById(httpClient);
    const postId = '1';

    const actualPost = await getPostDetailByIdService.execute(postId);

    expect(actualPost).toEqual(expectedPost);
  });
});

const post = {
  userId: 1,
  id: 1,
  title: 'title-1',
  body: 'body-1',
};

const comments = [
  {
    postId: 1,
    id: 1,
    name: 'id labore ex et quam laborum',
    email: 'Eliseo@gardner.biz',
    body: 'laudantium',
  },
];

const expectedPost = {
  id: 1,
  title: 'title-1',
  body: 'body-1',
  comments: [
    {
      id: 1,
      email: 'Eliseo@gardner.biz',
      body: 'laudantium',
    },
  ],
};
