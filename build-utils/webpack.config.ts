/* eslint-disable @typescript-eslint/no-var-requires */
import { merge } from 'webpack-merge';
import webpackCommonConfig from './webpack.common.config';

const getWebpackConfigByEnv = (env: string) =>
  require(`./webpack.${env}.config.ts`).default;

export default (env: { mode: string }): any => {
  return merge(webpackCommonConfig, getWebpackConfigByEnv(env.mode));
};
