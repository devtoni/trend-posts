import * as path from 'path';
import * as webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const ROOT_PATH = path.join(__dirname, '../');
const PUBLIC_ASSETS_PATH = '/';

const config: webpack.Configuration = {
  entry: path.resolve(ROOT_PATH, 'src/index.tsx'),
  output: {
    clean: true,
    path: path.resolve(ROOT_PATH, 'dist'),
    filename: '[name].[contenthash].bundle.js',
    publicPath: PUBLIC_ASSETS_PATH,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/i,
        exclude: /node_modules/,
        use: 'ts-loader',
      },
      {
        test: /\.woff2/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name][ext]',
        },
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
    alias: {
      '@styles': path.join(ROOT_PATH, 'src/styles'),
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(ROOT_PATH, 'public/index.html'),
      publicPath: PUBLIC_ASSETS_PATH,
      favicon: path.resolve(ROOT_PATH, 'public/favicon.ico'),
    }),
  ],
};

export default config;
