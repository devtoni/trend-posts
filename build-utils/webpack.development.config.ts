import * as webpack from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';

type Configuration = webpack.Configuration & {
  devServer: WebpackDevServerConfiguration;
};

const config: Configuration = {
  mode: 'development',
  devServer: {
    hot: true,
    port: 3000,
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              additionalData: `@import '@styles/global';`,
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
};

export default config;
