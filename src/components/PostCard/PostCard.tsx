import { Link } from 'react-router-dom';
import { Post } from '../../models/Post';

import './post-card.scss';

type Props = {
  post: Post;
};

function PostCard({ post }: Props): JSX.Element {
  return (
    <div className="post-card">
      <Link to={`/posts/${post.id}`} className="post-card__title">
        {post.title}
      </Link>
      <p className="post-card__summary">{post.body}.</p>
    </div>
  );
}

export default PostCard;
