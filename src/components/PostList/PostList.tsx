import { useCallback, useEffect, useState } from 'react';
import { Post } from '../../models/Post';
import getPostsService from '../../services/GetPosts';
import PostCard from '../PostCard/PostCard';

function PostList(): JSX.Element {
  const [isLoading, setIsLoading] = useState(false);
  const [posts, setPosts] = useState<Post[]>([]);

  const getPosts = useCallback(async () => {
    setIsLoading(true);

    const posts = await getPostsService().execute();

    setPosts(posts);

    setIsLoading(false);
  }, []);

  useEffect(() => {
    getPosts();
  }, [getPosts]);

  return isLoading ? (
    <p>Loading posts...</p>
  ) : (
    <section>
      {posts.map((post: Post) => {
        return <PostCard post={post} key={post.id} />;
      })}
    </section>
  );
}

export default PostList;
