import React from 'react';
import { PostComment } from '../../models/Post';

import './post-comment.scss';

type Props = {
  comment: PostComment;
};

function PostComment({ comment }: Props): JSX.Element {
  return (
    <li className="post-comment">
      <p className="post-comment__author">{comment.email}</p>
      <p className="post-comment__content">{comment.body}</p>
    </li>
  );
}

export default PostComment;
