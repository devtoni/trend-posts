import React, { useCallback, useEffect, useState } from 'react';
import { Post, PostComment as PostCommentModel } from '../../models/Post';
import getPostDetailByIdFactory from '../../services/GetPostDetailById';
import PostComment from '../PostComment/PostComment';

import './post-detail.scss';

type Props = {
  postId: string;
};

function PostDetail({ postId }: Props): JSX.Element {
  const [post, setPost] = useState<Post>();
  const [isLoading, setIsLoading] = useState(false);

  const getPostDetail = useCallback(async () => {
    setIsLoading(true);

    const post = await getPostDetailByIdFactory().execute(postId);

    setPost(post);

    setIsLoading(false);
  }, []);

  useEffect(() => {
    getPostDetail();
  }, [getPostDetail]);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <section className="post-detail">
      <div className="post-detail__content">
        <h1 className="post-detail__title">{post?.title}</h1>
        <p>{post?.body}</p>
      </div>
      <ul>
        {post?.comments?.map((comment: PostCommentModel) => (
          <PostComment comment={comment} key={comment.id} />
        ))}
      </ul>
    </section>
  );
}

export default PostDetail;
