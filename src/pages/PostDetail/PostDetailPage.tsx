import React from 'react';
import { useParams } from 'react-router';
import PostDetail from '../../components/PostDetail/PostDetail';

import './post-detail-page.scss';

type Params = {
  postId: string;
};

function PostPage(): JSX.Element {
  const { postId } = useParams<Params>();

  return (
    <div className="post-detail-page">
      <PostDetail postId={postId} />
    </div>
  );
}

export default PostPage;
