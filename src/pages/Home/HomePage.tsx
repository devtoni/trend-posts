import React from 'react';
import PostList from '../../components/PostList/PostList';

import './home-page.scss';

function Home(): JSX.Element {
  return (
    <div className="home-page">
      <div className="home-page__posts">
        <PostList />
      </div>
    </div>
  );
}

export default Home;
