import FetchClient from '../infrastructure/http/FetchClient';
import { HttpClient } from '../models/http/HttpClient';
import { Post, PostComment } from '../models/Post';

type ApiPost = Record<string, any>;

type ApiPostComment = Record<string, any>;

type ApiAggregatedPost = Record<string, any>;

class GetPostDetailById {
  private base = 'https://jsonplaceholder.typicode.com';

  constructor(private httpClient: HttpClient) {}

  async execute(postId: string): Promise<Post> {
    const [post, comments] = await Promise.all([
      this.getPostById(postId),
      this.getCommentsByPostId(postId),
    ]);

    return this.fromApiAggregatedPostToPostDetail({ ...post, comments });
  }

  private getPostById(postId: string): Promise<ApiPost> {
    const endpoint = `${this.base}/posts/${postId}`;

    return this.httpClient.get<ApiPost>(endpoint);
  }

  private getCommentsByPostId(postId: string): Promise<ApiPost> {
    const endpoint = `${this.base}/posts/${postId}/comments`;

    return this.httpClient.get<ApiPost>(endpoint);
  }

  private fromApiPostCommentToComment(
    apiPostComment: ApiPostComment
  ): PostComment {
    return {
      id: apiPostComment.id,
      email: apiPostComment.email,
      body: apiPostComment.body,
    };
  }

  private fromApiAggregatedPostToPostDetail(
    apiAggreagatedPost: ApiAggregatedPost
  ): Post {
    return {
      id: apiAggreagatedPost.id,
      title: apiAggreagatedPost.title,
      body: apiAggreagatedPost.body,
      comments: apiAggreagatedPost.comments.map(
        this.fromApiPostCommentToComment
      ),
    };
  }
}

export default (): GetPostDetailById =>
  new GetPostDetailById(new FetchClient());

export { GetPostDetailById };
