import FetchClient from '../infrastructure/http/FetchClient';
import { HttpClient } from '../models/http/HttpClient';
import { Post } from '../models/Post';

type ApiPost = Record<string, any>;

class GetPosts {
  private base = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private httpClient: HttpClient) {}

  async execute(): Promise<Post[]> {
    const result = await this.httpClient.get<ApiPost[]>(this.base);

    return this.fromApiPostsToPosts(result);
  }

  private fromApiPostsToPosts(apiPosts: ApiPost[]): Post[] {
    return apiPosts.map((post: ApiPost) => {
      return {
        id: post.id,
        title: post.title,
        body: post.body,
      };
    });
  }
}

export default (): GetPosts => new GetPosts(new FetchClient());

export { GetPosts };
