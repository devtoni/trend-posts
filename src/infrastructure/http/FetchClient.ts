import { HttpClient } from '../../models/http/HttpClient';

class FetchClient implements HttpClient {
  get<T>(url: string): Promise<T> {
    return fetch(url).then((res) => res.json());
  }
}

export default FetchClient;
