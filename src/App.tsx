import React from 'react';
import { Route, Switch } from 'react-router';

const HomePageComponent = React.lazy(
  () => import(/* webpackChunkName: "Home" */ './pages/Home/HomePage')
);

const PostPageComponent = React.lazy(
  () =>
    import(
      /* webpackChunkName: "PostDetail" */ './pages/PostDetail/PostDetailPage'
    )
);

function App(): JSX.Element {
  return (
    <React.Suspense fallback={<p>Loading...</p>}>
      <Switch>
        <Route path="/" component={HomePageComponent} exact />
        <Route path="/posts/:postId" component={PostPageComponent} exact />
      </Switch>
    </React.Suspense>
  );
}

export default App;
