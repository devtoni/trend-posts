type PostComment = {
  id: number;
  email: string;
  body: string;
};

type Post = {
  id: number;
  title: string;
  body: string;
  comments?: PostComment[];
};

export type { Post, PostComment };
