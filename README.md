# Trend Posts

Trend posts is an application where you can see a lists of trending posts and see its content

## Project configuration

### Technical requirements

- NodeJS v14.18 with Npm > 7

### Install project

From the root of the project type:

```
npm install
```

By running this command you will install all the needed dependencies for development and build the app for production.

### Start development environment

From the root of the project type:

```
npm start
```

This command will start a new process with webpack-dev-server and boot the application in localhost:3000

### Build project

To build the production code, from the root of the project type:

```
npm run build
```

This command will take all your src files and bundle them into a one transpiled file

### Unit tests

To run the unit tests, from the root of the project type:

```
npm run test or  npm run test:watch (watching mode)
```

### Run project in production mode (local purpose)

You can run the project locally as if it was in production environment by running the following command from the root of the project:

```
npm run serve
```

This command will build all the assets in production mode (webpack mode) and serve via http-server library